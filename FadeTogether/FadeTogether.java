package lfso;
import robocode.*;

//import static robocode.util.Utils.normalRelativeAngleDegrees;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * FadeTogether - a robot by Fernando Oliveira
 */
public class FadeTogether extends Robot
{
	int emf = 100;
	int eng = 100;
	public void run() {
		// Initialization of the robot should be put here

		setColors(Color.red,Color.blue,Color.green); // body,gun,radar
		
		// Robot main loop
		while(true) {
			ahead(emf);
			turnGunRight(360);
			back(eng);
			turnGunRight(360);
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		
		fire(1);
		turnGunLeft(45);
		scan();
		
		if (e.getDistance() < 200) {
			if (e.getBearing() > -90 && e.getBearing() <= 90) {
				back(40);
			} else {
				ahead(40);
			}
		}
	}

	public void onHitByBullet(HitByBulletEvent e) {
		turnLeft(90 - e.getBearing());
		ahead(100);
		if (emf <= 250) {
			emf += 20;
			eng += 20;
		} else {
			emf -= 75;
			eng -= 75;
		}
	}
	
	public void onHitWall(HitWallEvent e) {
		turnLeft(90 - e.getBearing());
		ahead(200);
	}	
}
