# Esse é o FadeTogether!

Olá! Eu sou o Fernando e esse é meu ReadMe sobre meu robô do Robocode, o FadeTogether. Um robô bem simples, quase um Frankenstein de outros bots, com algumas personalizações próprias para torná-lo um pouco mais eficaz. Agora falarei um pouco de cada um dos métodos dele:


## run()

O método básico do robô, onde um while(true) faz com que ele esteja sempre em movimento e escaneando ao seu redor. Ele utiliza duas variáveis no ahead() e no back() para que o valor seja alterado conforme alguns métodos posteriores.

## onScannedRobot()

Novamente, muito simples, atira uma bullet de potência 1, para não haver um gasto excessivo de energia, em seguida ele vira a arma 45º para a esquerda antes de escanear novamente, isso permite que ele escaneie o robô adversário com uma frequência um pouco maior que o normal.

Em seguida, há um e.getDistance(), para tentar manter alguma distância do robô adversário - essa é uma das partes que gostaria de otimizar, visto que ainda sofro com o RamFire.

## onHitByBullet()

O robô vira 90º para esquerda e anda para frente antes de retornar à movimentação normal. Além disso, as variáveis de movimentação sofrem algumas alterações com if else

## onHitWall()

Por último, caso ele colida com alguma parede, ele vira 90º e anda, isso permite que ele acompanhe a parede por algum momento escapando de tiros (anteriormente ele andava na direção oposta da parede, mas tornava-se um alvo fácil).